ARG NPM_VERSION 9.3.1
FROM node:16.19.0

RUN npm install -g npm@${NPM_VERSION} typescript

RUN mkdir /uciranks

WORKDIR /uciranks

COPY src/* /uciranks/src/
COPY tests/ /uciranks/tests/
COPY *.ts /uciranks/
COPY *.json /uciranks/
COPY api/ /uciranks/api/
COPY tests/ /uciranks/tests/

RUN npm install

RUN npx ts-mocha tests/*.test.ts