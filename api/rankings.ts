'use strict'

import { PaginationDetails, RankingData, RankingRequest, RankingRider } from "../src/types";

import fetch from 'node-fetch';

const DATARIDE_URL: string = "https://dataride.uci.ch/iframe/ObjectRankings/";
const REFERRER_URL: string = "https://dataride.uci.ch/iframe/RankingDetails"
const DEFAULT_HEADERS: any = {
    "accept": "application/json, text/javascript, */*; q=0.01",
    "accept-language": "en-AU,en-GB;q=0.9,en-US;q=0.8,en;q=0.7",
    "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
    "sec-ch-ua": "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"",
    "sec-ch-ua-mobile": "?0",
    "sec-ch-ua-platform": "\"Windows\"",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-origin",
    "x-requested-with": "XMLHttpRequest",
    "Referrer-Policy": "strict-origin-when-cross-origin"
  }

const createFilterLine = (id: string, value: number|string): string => {
    return "filter%5Bfilters%5D%5B0%5D%5Bfield%5D=" + id + 
    "&filter%5Bfilters%5D%5B0%5D%5Bvalue%5D=" + value.toString();
}

const paginationDetails = (pagination: PaginationDetails): string => {
    return `take=${pagination.take}&skip=${pagination.skip}&page=${pagination.page}&pageSize=${pagination.pageSize}&`;
}

export const createReferrerUrl = (request: RankingRequest): string => {
    const usp: URLSearchParams = new URLSearchParams(request as never);
    return usp.toString();
};

export default class Rankings {
    constructor() {

    }

    
    createFilterBody(request: RankingRequest): string {
        let filterLine: string = `rankingId=${request.rankingId}&disciplineId=${request.disciplineId}&rankingTypeId=${request.rankingTypeId}&` + 
            request.pagination ? paginationDetails(request.pagination!) : '' + 
            createFilterLine('RaceTypeId', request.raceTypeId) + "&" + 
            createFilterLine('CategoryId', request.categoryId) + "&" + 
            createFilterLine('SeasonId', request.disciplineSeasonId) + "&" + 
            createFilterLine('MomentId', request.momentId);
        if (request.countryId) {
            filterLine += '&' + createFilterLine('CountryId', request.countryId);
        }
        filterLine += '&' + createFilterLine('IndividualName', request.individualName || '');
        filterLine += '&' + createFilterLine('TeamName', request.teamName || '');
        return filterLine;
    }

    getRankings(discipline:string, category:string):any {
        const requestData: RankingRequest = {
            rankingId: 148,
            disciplineId: 7,
            groupId: 35,
            momentId: 156894,
            disciplineSeasonId: 420,
            rankingTypeId: 1,
            categoryId: 22,
            raceTypeId: 92
        };
        const usp: URLSearchParams = new URLSearchParams(requestData as never);
        
        fetch(DATARIDE_URL, {
            "headers": {
                ...DEFAULT_HEADERS,
                Referrer: `${REFERRER_URL}/148?${createReferrerUrl(requestData)}`
                //disciplineId=7&groupId=35&momentId=156894&disciplineSeasonId=420&rankingTypeId=1&categoryId=22&raceTypeId=92`,
            },
            "body": this.createFilterBody({
                ...requestData,
                countryId: 14,
                pagination: {
                    take: 40,
                    skip: 0,
                    page: 1,
                    pageSize: 40,
                }
            }),
            "method": "POST"
          });

    }

    getPerson(rider: RankingRider): RankingData {
        return {} as RankingData;
    }
}