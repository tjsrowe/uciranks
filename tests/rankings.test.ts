import { RankingRequest } from '../src/types';
import assert from 'assert';
import { createReferrerUrl } from '../api/rankings';

describe('Should create a SearchURL', () => {
  it('Create a url', () => {
    let requestData: RankingRequest;
    
    requestData = {
      disciplineId: 7,
      groupId: 35,
      momentId: 156894,
      disciplineSeasonId: 420,
      rankingTypeId: 1,
      categoryId: 22,
      raceTypeId: 92
    };
    // const usp: URLSearchParams = new URLSearchParams(requestData as never);
    const usp: string = createReferrerUrl(requestData);
    assert.equal(usp, "disciplineId=7&groupId=35&momentId=156894&disciplineSeasonId=420&rankingTypeId=1&categoryId=22&raceTypeId=92");
  });
});
