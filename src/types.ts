export type UCIID = string;

export interface RankingRider {
    firstname: string,
    surname: string,
    uciId: UCIID,
};

interface IRankingData extends RankingRider {
    points: number,
    rank: number
}

export type RankingData = IRankingData;

export interface PaginationDetails {
  take: number,
  skip: number,
  page: number,
  pageSize: number
}

interface IRankingRequest {
    rankingId?: number,
    disciplineId: number,
    groupId: number,
    momentId: number,
    disciplineSeasonId: number,
    rankingTypeId: number,
    categoryId: number,
    raceTypeId: number,
    countryId?: number,
    teamName?: string,
    individualName?: string,
    pagination?: PaginationDetails
}

export type RankingRequest = IRankingRequest;