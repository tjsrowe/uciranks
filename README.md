# UCIRanks README #

This repository is intended to be the future home of the UCI MTB rankings tools to be used for race start list generation.  The intent of the project is to retrieve the data from the UCI website, which is provided in an extremely un-friendly format for data-processing, and present this in a simple-to-access REST API, caching that data and providing it for offline use where necessary.  This would then allow events which utilise the UCI rules to automatically match up rider data based on their UCI ID and/or Name/DOB, depending on what data is correctly provided.  This data can then be married to other ranking data, notably that which comes from the pointsseries and racesecretary project, such as when running races which are part of the AusCycling National Cup.

