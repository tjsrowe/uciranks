#!/bin/sh

. $(dirname "$0")/../ci/env.sh

if [ -z "$BITBUCKET_COMMIT" ]; then
  echo 'BITBUCKT_COMMIT must be set - usually comes from CI process'
  exit 2
fi

IMAGE_NAME=uciranks:$BITBUCKET_COMMIT

DOCKER_BUILDKIT=1 docker build -f Dockerfile -t $IMAGE_NAME .
