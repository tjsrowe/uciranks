'use strict'

const SERVER_PORT:number = process.env['PORT'] ? parseInt(process.env['PORT']) : 4320;

import Rankings from './api/rankings';
import express from 'express';

const apiServer = express();
const rankings:Rankings = new Rankings();

apiServer.get("/:discipline/:category", (request, response) => {
    const data:Promise<any> = rankings.getRankings(request.params.discipline, request.params.category);
    data.then((output) => {
        response.send(output);
        response.end();
    })
});

apiServer.listen(SERVER_PORT, () => {
    console.log(`Server started at http://localhost:${SERVER_PORT}`);
});

export * from './src/types';